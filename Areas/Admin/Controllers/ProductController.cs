﻿using ChieuT6.Models;
using ChieuT6.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ChieuT6.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class ProductController
    {
        private readonly IproductRepository _productRepository;
        private readonly IcategoryRepository _categoryRepository;
    }
}
