using ChieuT6.Models;
using ChieuT6.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace E4Lap03.Controllers
{
    public class HomeController : Controller
    {
        private readonly IproductRepository _productRepository;

        public HomeController(IproductRepository productRepository)
        {
            _productRepository = productRepository;
        }
       // private readonly ILogger<HomeController> _logger;

        /* public HomeController(ILogger<HomeController> logger)
         {
             _logger = logger;
         }
        */


        public async Task<IActionResult> Index()
        {
            var products = await _productRepository.GetAllAsync();
            return View(products);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
